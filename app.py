from flask import Flask, request
from flask.json import jsonify
import json
import sqlite3
import time
import numpy as np

app = Flask(__name__)

# Setup the SQLite DB
conn = sqlite3.connect('database.db')
conn.execute('CREATE TABLE IF NOT EXISTS readings (device_uuid TEXT, type TEXT, value INTEGER, date_created INTEGER)')
conn.close()


@app.route('/devices/<string:device_uuid>/readings/', methods=['POST', 'GET'])
def request_device_readings(device_uuid):
    """
    This endpoint allows clients to POST or GET data specific sensor types.

    POST Parameters:
    * type -> The type of sensor (temperature or humidity)
    * value -> The integer value of the sensor reading
    * date_created -> The epoch date of the sensor reading.
        If none provided, we set to now.

    Optional Query Parameters:
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    * type -> The type of sensor value a client is looking for
    """

    # Set the db that we want and open the connection
    if app.config['TESTING']:
        conn = sqlite3.connect('test_database.db')
    else:
        conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()

    if request.method == 'POST':
        # Grab the post parameters
        post_data = json.loads(request.data)
        sensor_type = post_data.get('type')
        value = post_data.get('value')
        date_created = post_data.get('date_created', int(time.time()))
        if not (100 >= value >= 0):
            return jsonify('Bad Request'), 400
        # Insert data into db
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (device_uuid, sensor_type, value, date_created))
        conn.commit()
        # Return success
        return jsonify('success'), 201
    else:
        sql_query_string = "SELECT * FROM readings WHERE device_uuid='{}'".format(device_uuid)
        sensor_type = request.args.get('type')
        start = request.args.get('start')
        end = request.args.get('end')
        if sensor_type is not None:
            sql_query_string += " AND type='{}'".format(sensor_type)
        if start is None:
            start = 1
        if end is None:
            end = int(time.time())
        sql_query_string += " AND date_created BETWEEN {} AND {} ".format(start, end)
        print(sql_query_string)
        cur.execute(sql_query_string)
        rows = cur.fetchall()
        # Return the JSON
        return jsonify([dict(zip(['device_uuid', 'type', 'value', 'date_created'], row)) for row in rows]), 200


@app.route('/devices/<string:device_uuid>/readings/max/', methods=['GET'])
def request_device_readings_max(device_uuid):
    """
    This endpoint allows clients to GET the max sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    if not request.args.get('type'):
        return 'Bad Request', 400
    sensor_type = request.args.get('type')
    start = request.args.get('start')
    end = request.args.get('end')
    return jsonify(get_max(device_uuid, sensor_type, start, end)), 200


@app.route('/devices/<string:device_uuid>/readings/median/', methods=['GET'])
def request_device_readings_median(device_uuid):
    """
    This endpoint allows clients to GET the median sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    if not request.args.get('type'):
        return 'Bad Request', 400
    sensor_type = request.args.get('type')
    start = request.args.get('start')
    end = request.args.get('end')
    return jsonify(get_metrics(device_uuid, sensor_type, start, end, 'median')), 200


@app.route('/devices/<string:device_uuid>/readings/mean/', methods=['GET'])
def request_device_readings_mean(device_uuid):
    """
    This endpoint allows clients to GET the mean sensor readings for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    if not request.args.get('type'):
        return 'Bad Request', 400
    sensor_type = request.args.get('type')
    start = request.args.get('start')
    end = request.args.get('end')
    return jsonify(get_metrics(device_uuid, sensor_type, start, end, 'mean')), 200


@app.route('/devices/<string:device_uuid>/readings/quartiles/', methods=['GET'])
def request_device_readings_quartiles(device_uuid):
    """
    This endpoint allows clients to GET the 1st and 3rd quartile
    sensor reading value for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    if not request.args.get('type'):
        return 'Bad Request', 400
    if not request.args.get('start'):
        return 'Bad Request', 400
    if not request.args.get('end'):
        return 'Bad Request', 400
    sensor_type = request.args.get('type')
    start = request.args.get('start')
    end = request.args.get('end')
    return jsonify(get_metrics(device_uuid, sensor_type, start, end, 'quartiles')), 200


@app.route('/devices/readings/summary/', methods=['GET'])
def request_readings_summary():
    """
    This endpoint allows clients to GET a full summary
    of all sensor data in the database per device.

    Optional Query Parameters
    * type -> The type of sensor value a client is looking for
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    if app.config['TESTING']:
        conn = sqlite3.connect('test_database.db')
    else:
        conn = sqlite3.connect('database.db')
    sensor_type = request.args.get('type')
    start = request.args.get('start')
    end = request.args.get('end')
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    where_flag = False
    sql_query_string = "SELECT device_uuid, type, count(*) FROM readings "
    sql_additional_conditions = ""
    sql_group_by = " GROUP BY device_uuid, type"
    if sensor_type is not None:
        where_flag = True
        sql_additional_conditions = " WHERE type='{}'".format(sensor_type)
    if start is None:
        start = 1
    if end is None:
        end = int(time.time())
    if where_flag is True:
        sql_additional_conditions += " AND date_created BETWEEN '{}' AND '{}' ".format(start, end)
    else:
        sql_additional_conditions += " WHERE date_created BETWEEN '{}' AND '{}' ".format(start, end)
    cur.execute(sql_query_string + sql_additional_conditions + sql_group_by)
    dict_devices = [dict(zip(['device_uuid', 'type', 'number_of_readings'], row)) for row in cur.fetchall()]
    summary_list = []
    for device in dict_devices:
        max_json = get_max(device['device_uuid'], device['type'], None, None)
        median_json = get_metrics(device['device_uuid'], device['type'], None, None, 'median')
        mean_json = get_metrics(device['device_uuid'], device['type'], None, None, 'mean')
        quartiles_json = get_metrics(device['device_uuid'], device['type'], None, None, 'quartiles')
        device_summary = dict(device_uuid=device['device_uuid'], number_of_readings=device['number_of_readings'],
                              max_reading_value=max_json[0]['value'], median_reading_value=median_json['value'],
                              mean_reading_value=mean_json['value'],
                              quartile_1_value=quartiles_json['quartile_1'],
                              quartile_3_value=quartiles_json['quartile_3'])
        summary_list.append(device_summary)
    return jsonify(summary_list), 200


def get_metrics(device_uuid, sensor_type, start, end, metrics):
    """
    This function is called by mean, median, quartile and devices summary endpoints.

    Parameters
    * device_uuid -> device uuid for which the data is requested
    * sensor_type -> The type of sensor value a client is looking for
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    * metrics -> Is string ,possible values
        'median' , 'mean', 'quartiles'
    """
    if app.config['TESTING']:
        conn = sqlite3.connect('test_database.db')
    else:
        conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    sql_query_string = "SELECT value FROM readings WHERE device_uuid=? AND type=?"
    if start is None:
        start = 1
    if end is None:
        end = int(time.time())
    sql_query_string += " AND date_created BETWEEN ? AND ? "
    cur.execute(sql_query_string, (device_uuid, sensor_type, start, end,))
    values = [sensor_values[0] for sensor_values in cur.fetchall()]
    if metrics == 'median':
        values.sort()
        return dict(device_uuid=device_uuid, type=sensor_type, value=np.quantile(values, .50))
    elif metrics == 'quartiles':
        values.sort()
        return dict(quartile_1=np.quantile(values, .25), quartile_3=np.quantile(values, .75))
    elif metrics == 'mean':
        return dict(value=np.mean(values))
    return 'Bad Request'


def get_max(device_uuid, sensor_type, start, end):
    """
    This function is called by max and devices summary endpoints.

    Parameters
    * device_uuid -> device uuid for which the data is requested
    * sensor_type -> The type of sensor value a client is looking for
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    if app.config['TESTING']:
        conn = sqlite3.connect('test_database.db')
    else:
        conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    sql_query_string = "SELECT device_uuid, type, max(value), date_created FROM readings WHERE device_uuid=? AND type=?"
    if start is None:
        start = 1
    if end is None:
        end = int(time.time())
    sql_query_string += " AND date_created BETWEEN ? AND ? "
    cur.execute(sql_query_string, (device_uuid, sensor_type, start, end,))
    return [dict(zip(['device_uuid', 'type', 'value', 'date_created'], row)) for row in cur.fetchall()]


if __name__ == '__main__':
    app.run()
