import json
import pytest
import sqlite3
import time
import unittest

from app import app


class SensorRoutesTestCases(unittest.TestCase):

    def setUp(self):
        # Setup the SQLite DB
        conn = sqlite3.connect('test_database.db')
        conn.execute('DROP TABLE IF EXISTS readings')
        conn.execute(
            'CREATE TABLE IF NOT EXISTS readings (device_uuid TEXT, type TEXT, value INTEGER, date_created INTEGER)')

        self.device_uuid = 'test_device'

        # Setup some sensor data
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()

        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (self.device_uuid, 'temperature', 22, int(time.time()) - 100))
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (self.device_uuid, 'temperature', 50, int(time.time()) - 50))
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (self.device_uuid, 'temperature', 100, int(time.time())))

        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    ('other_uuid', 'temperature', 22, int(time.time())))
        conn.commit()

        app.config['TESTING'] = True

        self.client = app.test_client

    def test_device_readings_get(self):
        # Given a device UUID
        # When we make a request with the given UUID
        request = self.client().get('/devices/{}/readings/'.format(self.device_uuid))

        # Then we should receive a 200
        self.assertEqual(request.status_code, 200)

        # And the response data should have three sensor readings
        self.assertTrue(len(json.loads(request.data)) == 3)

    def test_device_readings_post(self):
        # Given a device UUID
        # When we make a request with the given UUID to create a reading
        request = self.client().post('/devices/{}/readings/'.format(self.device_uuid), data=json.dumps({
            'type': 'temperature',
            'value': 100
        }))

        # Then we should receive a 201
        self.assertEqual(request.status_code, 201)

        # And when we check for readings in the db
        conn = sqlite3.connect('test_database.db')
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute('select * from readings where device_uuid="{}"'.format(self.device_uuid))
        rows = cur.fetchall()

        # We should have three
        self.assertTrue(len(rows) == 4)

    def test_device_readings_post_out_of_range_value(self):
        # Given a device UUID
        # When we make a request with the given UUID to create a reading
        request = self.client().post('/devices/{}/readings/'.format(self.device_uuid), data=json.dumps({
            'type': 'temperature',
            'value': 101
        }))

        # Then we should receive a 400
        self.assertEqual(request.status_code, 400)

    def test_device_readings_get_temperature(self):
        request = self.client().get('/devices/{}/readings/?type=temperature'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        print(len(json.loads(request.data)))
        self.assertTrue(len(json.loads(request.data)) == 3)

    def test_device_readings_get_humidity(self):
        request = self.client().get('/devices/{}/readings/?type=humidity'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 0)

    def test_device_readings_get_past_dates(self):
        request = self.client().get('/devices/{}/readings/?start=1&end="{}"'.format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 3)

    def test_device_readings_min(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's min sensor reading.
        """
        self.assertTrue(False)

    def test_device_readings_max(self):
        request = self.client().get('/devices/{}/readings/max/?type=temperature'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 1)

    def test_device_readings_max_missing_type_in_query(self):
        request = self.client().get('/devices/{}/readings/max/'.format(self.device_uuid))
        self.assertEqual(request.status_code, 400)

    def test_device_readings_max_past_dates(self):
        request = self.client().get('/devices/{}/readings/max/?type=temperature&start=1&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 1)

    def test_device_readings_max_missing_end_in_query(self):
        request = self.client().get('/devices/{}/readings/max/?type=temperature&start=1'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 1)

    def test_device_readings_median(self):
        request = self.client().get('/devices/{}/readings/median/?type=temperature'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        # return json has 3 keys:value pairs
        self.assertTrue(len(json.loads(request.data)) == 3)

    def test_device_readings_median_missing_type_in_query(self):
        request = self.client().get('/devices/{}/readings/median/'.format(self.device_uuid))
        self.assertEqual(request.status_code, 400)

    def test_device_readings_median_past_dates(self):
        request = self.client().get('/devices/{}/readings/median/?type=temperature&start=1&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # return json has 3 keys:value pairs
        self.assertTrue(len(json.loads(request.data)) == 3)

    def test_device_readings_median_missing_end_in_query(self):
        request = self.client().get('/devices/{}/readings/median/?type=temperature&start=1'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 3)

    def test_device_readings_mean(self):
        request = self.client().get('/devices/{}/readings/mean/?type=temperature'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 1)

    def test_device_readings_mean_missing_type_in_query(self):
        request = self.client().get('/devices/{}/readings/mean/'.format(self.device_uuid))
        self.assertEqual(request.status_code, 400)

    def test_device_readings_mean_missing_end_in_query(self):
        request = self.client().get('/devices/{}/readings/mean/?type=temperature&start=1'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        self.assertTrue(len(json.loads(request.data)) == 1)

    def test_device_readings_mean_past_dates(self):
        request = self.client().get('/devices/{}/readings/mean/?type=temperature&start=1&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # return json has 3 keys:value pairs
        self.assertTrue(len(json.loads(request.data)) == 1)

    def test_device_readings_mode(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's mode sensor reading value.
        """
        self.assertTrue(False)

    def test_device_readings_quartiles(self):
        request = self.client().get('/devices/{}/readings/quartiles/?type=temperature&start=0&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # return json has 2 keys:value pairs
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_device_readings_quartiles_missing_type_in_query(self):
        request = self.client().get('/devices/{}/readings/quartiles/?start=1&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 400)

    def test_device_readings_quartiles_missing_start_in_query(self):
        request = self.client().get('/devices/{}/readings/quartiles/?type=temperature&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 400)

    def test_device_readings_quartiles_missing_end_in_query(self):
        request = self.client().get('/devices/{}/readings/quartiles/?type=temperature&start=1'.format(self.device_uuid))
        self.assertEqual(request.status_code, 400)

    def test_readings_summary(self):
        request = self.client().get('/devices/readings/summary/'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_temperature(self):
        request = self.client().get('/devices/readings/summary/?type=temperature'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_humidity(self):
        request = self.client().get('/devices/readings/summary/?type=humidity'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices but no humidity data
        self.assertTrue(len(json.loads(request.data)) == 0)

    def test_readings_summary_get_type_and_start_and_end(self):
        request = self.client().get('/devices/readings/summary/?type=temperature&start=1&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_start_and_end(self):
        request = self.client().get('/devices/readings/summary/?start=1&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_type_and_start(self):
        request = self.client().get('/devices/readings/summary/?type=temperature&start=1'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_start(self):
        request = self.client().get('/devices/readings/summary/?start=1'.format(self.device_uuid))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_type_(self):
        request = self.client().get('/devices/readings/summary/?type=temperature&end="{}"'
                                    .format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)

    def test_readings_summary_get_end(self):
        request = self.client().get('/devices/readings/summary/?end="{}"'.format(self.device_uuid, int(time.time())))
        self.assertEqual(request.status_code, 200)
        # Test db has 2 devices and just temperature data
        self.assertTrue(len(json.loads(request.data)) == 2)
